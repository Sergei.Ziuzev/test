variable "do_token" {}    # DigitalOcean Personal Access Token
variable "pvt_key" {}     # DigitalOcean Privete key location
variable "aws_pvt_key" {} # AWS Privete key location
variable "access_key" {}  # AWS Access key
variable "secret_key" {}  # AWS secret key

# AWS region
variable "aws_region" {
  type = string
  description = "AWS region"
}

# DO region
variable "region" {
  type = string
  description = "Region"
}

variable "vpc_name" {
  type = string
  description = "VPC name"
}

variable "ssh_name" {
  type = string
  description = "name of ssh key"
}

# -- Describe web server
variable "web_name" {
  type = string
  description = "Web server name"
}
variable "web_image" {
  type = string
  description = "web server image"
}
variable "web_size" {
  type = string
  description = "web server size"
}
variable "web_tag" {
  type = string
  description = "web server tag"
}
variable "username" {
  type = string
  description = "Non root user"
}