# Use DigitalOcean provider
terraform {
  required_version = "~> 1.2.5"
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}
provider "aws" {
  region = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
}