output "instance_ip" {
  value = digitalocean_droplet.web.ipv4_address
}
output "loadbalancer_ip" {
  value = digitalocean_loadbalancer.lb.ip
}