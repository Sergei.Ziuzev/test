resource "aws_vpc" "my_vpc" {
  cidr_blokcidr_block = "172.16.0.0/16"
  tags = {
    Name = "web-server"
  }
}
resource "aws_subnet" "web" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "172.16.10.0/24"
  availability_zone = "us-west-2a"
  tags = {
    Name = "web-server"
  }
}

data "aws_key_pair" "web" {
  key_name = "AWSKey"
  incude_public_key = true
}

resource "aws_instance" "web-server" {
  ami = "ami-0d70546e43a941d70"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.web.id
  key_name = data.aws_key_pair.web.id
  security_groups = [ "${aws_security_group.web-server.name}" ]

  connection {
    type = "ssh"
    host = self.public_ip
    user = "root"
    private_key = file(var.aws_pvt_key)
  }

  # Add non-root user
  provisioner "file" {
    source = "../add_user.sh"
    destination = "/tmp/add_user.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/add_user.sh",
      "/tmp/add_user.sh ${var.username}",
    ]
  }
  # Setup Docker
  provisioner "file" {
    source = "../setup_docker.sh"
    destination = "/tmp/setup_docker.sh"
  }
  provisioner "remote-exec" {
    # script = locals.setup_docker
    inline = [
      "chmod +x /tmp/setup_docker.sh",
      "/tmp/setup_docker.sh ${var.username}",
    ]
  }


  tags = {
    Name = "web-server"
  }
}