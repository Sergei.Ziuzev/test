resource "aws_security_group" "web-server" {
  name = "web-server"
  vpc_id = aws_vpc.my_vpc.id
  description = "Allo incoming HTTP/HTTPS connactions"

  ingress {
    description = "Port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  ingress {
    description = "Port 443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
}