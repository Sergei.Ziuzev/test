resource "aws_alb_target_group" "web" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }
  name          = "web"
  port          = 80
  protocol      = "HTTP"
  target_type   = "instance"
  vpc_id        = aws_vpc.my_vpc.id
}

resource "aws_alb" "alb-web" {
  name = "alb-web"
  internal = false
  ip_address_type = "ipv4"
  load_balancer_type = "application"
  security_groups = [aws_security_group.web_server.id]
  subnets = [aws_subnet.web.id]

}

resource "aws_lb_listener" "alb_lis" {
  load_balancer_arn = aws_alb.alb-web.arn
  port = 80
  protocol = "HTTP"
  default_action {
    target_group_arn = aws_lsb_target_group.web.arn
    type = "forward"
  }
}
resource "aws_lb_listener" "alb_lis_443" {
  load_balancer_arn = aws_alb.alb-web.arn
  port = 443
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-2016-08"
  certificate_arn = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    target_group_arn = aws_lsb_target_group.web.arn
    type = "forward"
  }
}

resource "aws_lb_target_group_attachment" "instance_attach" {
  target_group_arn = aws_lb_target_group.web.arn
  target_id = aws_instance.web-server.id
}