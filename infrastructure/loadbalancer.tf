resource "digitalocean_certificate" "cert" {
  name    = "custom-terraform"
  type    = "custom"
  private_key = file("certs/privkey1.pem")
  leaf_certificate  = file("certs/cert1.pem")
  certificate_chain = file("certs/chain1.pem")
}

resource "digitalocean_loadbalancer" "lb" {
  name = "lb"
  region = var.region

  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 80
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port = 443
    entry_protocol = "https"

    target_port = 80
    target_protocol = "http"

    certificate_name = digitalocean_certificate.cert.name
  }

  redirect_http_to_https = true

  healthcheck {
    port = 22
    protocol = "tcp"
  }
  droplet_ids = [ digitalocean_droplet.web.id ]
}