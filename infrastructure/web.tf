# Get VPC ID
data "digitalocean_vpc" "fra_vpc" {
  name = var.vpc_name
}

# Get ssh public key
data "digitalocean_ssh_key" "ssh_key_name" {
  name = var.ssh_name
}

# Create a web server
resource "digitalocean_droplet" "web" {
  image = var.web_image
  name = var.web_name
  region = var.region
  size = var.web_size
  ssh_keys = [ data.digitalocean_ssh_key.ssh_key_name.id ]
  vpc_uuid = data.digitalocean_vpc.fra_vpc.id
  tags = [ var.web_tag ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    agent = true
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  # Add non-root user
  provisioner "file" {
    source = "add_user.sh"
    destination = "/tmp/add_user.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/add_user.sh",
      "/tmp/add_user.sh ${var.username}",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "apt-get update",
      "apt-get install ca-certificates curl gnupg lsb-release",
      "mkdir -p /etc/apt/keyrings && cd /tmp",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg",
      "echo \"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null",
      "apt-get update && apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y",
      "usermod -aG docker ${var.username}"
    ]
  }
}