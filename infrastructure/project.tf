# Get id of project Test
data "digitalocean_project" "Test" {
  name = "Test"
}

resource "digitalocean_project_resources" "Test" {
  project = data.digitalocean_project.Test.id
  resources = [
      digitalocean_droplet.web.urn,
      digitalocean_loadbalancer.lb.urn
  ]
}